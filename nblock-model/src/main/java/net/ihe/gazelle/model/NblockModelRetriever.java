package net.ihe.gazelle.model;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NblockModelRetriever extends ModelRetriever{

    @Override
    public Map<String, URL> retrieveResources() {
        return null;
    }

    @Override
    public Map<String, URL> retrieveModels(){
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("nblock.uml", getClass().getResource("/models/nblock.uml"));
        return modelMap;
    }
}
