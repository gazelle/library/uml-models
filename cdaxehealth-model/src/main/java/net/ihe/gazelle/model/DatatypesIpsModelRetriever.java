package net.ihe.gazelle.model;

public class DatatypesIpsModelRetriever extends DatatypesModelRetriever{

    @Override
    protected String getDatatypeUmlName() {
        return "/datatypes-ips-model/datatypes.uml";
    }

    @Override
    protected String getDatatypeUmlKey() {
        return "datatypes.uml";
    }
}
