package net.ihe.gazelle.model;

public class InfrIpsModelRetriever extends InfrModelRetriever{

    @Override
    protected String getInfrUmlName() {
        return "/infr-ips-model/infr.uml";
    }

    @Override
    protected String getInfrUmlKey() {
        return "infr.uml";
    }
}
