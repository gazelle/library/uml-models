package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
public class CdaxehealthModelRetriever extends ModelRetriever {

    @Override
    public Map<String, URL> retrieveResources() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("attrDTs.xml", getClass().getResource("/cdaxehealth/attrDTs.xml"));
        modelMap.put("choicesDefault.xml", getClass().getResource("/cdaxehealth/choicesDefault.xml"));
        modelMap.put("datatypes.xml", getClass().getResource("/cdaxehealth/datatypes.xml"));
        modelMap.put("distinguishers.xml", getClass().getResource("/cdaxehealth/distinguishers.xml"));
        modelMap.put("modelsDefinition.xml", getClass().getResource("/cdaxehealth/modelsDefinition.xml"));
        modelMap.put("templatesTypeMapping.xml", getClass().getResource("/cdaxehealth/templatesTypeMapping.xml"));
        return modelMap;
    }

    @Override
    public Map<String, URL> retrieveModels() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("cda.uml", getClass().getResource("/modelxeh/cda.uml"));
        modelMap.put("pharm.uml", getClass().getResource("/modelxeh/pharm.uml"));
        modelMap.put("lab.uml", getClass().getResource("/modelxeh/lab.uml"));
        //modelMap.put("datatypes-ips.uml", getClass().getResource("/datatypes-ips-model/datatypes-ips.uml"));

        VocModelRetriever vocModelRetriever = new VocModelRetriever();
        Map<String, URL> vocModelMap = vocModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : vocModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        NblockModelRetriever nblockModelRetriever = new NblockModelRetriever();
        Map<String, URL> nblockModelsMap = nblockModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : nblockModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }



        DatatypesModelRetriever datatypesIpsModelRetriever = new DatatypesIpsModelRetriever();
        Map<String, URL> datatypesIpsModelMap = datatypesIpsModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : datatypesIpsModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }

        }

        InfrModelRetriever infrModelRetriever = new InfrIpsModelRetriever();
        Map<String, URL> infrModelMap = infrModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : infrModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        CommonModelRetriever commonModelRetriever = new CommonModelRetriever();
        Map<String, URL> commonModelsMap = commonModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : commonModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }
        return modelMap;
    }
}
