package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CommonModelRetriever extends ModelRetriever {

    @Override
    public Map<String, URL> retrieveResources() {
        return null;
    }

    @Override
    public Map<String, URL> retrieveModels(){
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("common.uml", getClass().getResource("/models/common.uml"));
        return modelMap;
    }
}
