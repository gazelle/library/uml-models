package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CdaModelRetriever extends ModelRetriever {

    @Override
    public Map<String, URL> retrieveResources() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("attrDTs.xml", getClass().getResource("/cdabasic/attrDTs.xml"));
        modelMap.put("choicesDefault.xml", getClass().getResource("/cdabasic/choicesDefault.xml"));
        modelMap.put("datatypes.xml", getClass().getResource("/cdabasic/datatypes.xml"));
        modelMap.put("distinguishers.xml", getClass().getResource("/cdabasic/distinguishers.xml"));
        modelMap.put("modelsDefinition.xml", getClass().getResource("/cdabasic/modelsDefinition.xml"));
        modelMap.put("templatesTypeMapping.xml", getClass().getResource("/cdabasic/templatesTypeMapping.xml"));
        return modelMap;
    }

    @Override
    public Map<String, URL> retrieveModels(){
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("cda.uml", getClass().getResource("/models/cda.uml"));


        VocModelRetriever vocModelRetriever = new VocModelRetriever();
        Map<String, URL> vocModelMap = vocModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : vocModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        NblockModelRetriever nblockModelRetriever = new NblockModelRetriever();
        Map<String, URL> nblockModelsMap = nblockModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : nblockModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        DatatypesModelRetriever datatypesModelRetriever = new DatatypesModelRetriever();
        Map<String, URL> datatypesModelMap = datatypesModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : datatypesModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        CommonModelRetriever commonModelRetriever = new CommonModelRetriever();
        Map<String, URL> commonModelsMap = commonModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : commonModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }
        return modelMap;
    }
}
