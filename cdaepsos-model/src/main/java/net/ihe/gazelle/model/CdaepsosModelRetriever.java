package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CdaepsosModelRetriever extends ModelRetriever {

    @Override
    public Map<String, URL> retrieveResources() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("attrDTs.xml", getClass().getResource("/cdaepsos/attrDTs.xml"));
        modelMap.put("choicesDefault.xml", getClass().getResource("/cdaepsos/choicesDefault.xml"));
        modelMap.put("datatypes.xml", getClass().getResource("/cdaepsos/datatypes.xml"));
        modelMap.put("distinguishers.xml", getClass().getResource("/cdaepsos/distinguishers.xml"));
        modelMap.put("modelsDefinition.xml", getClass().getResource("/cdaepsos/modelsDefinition.xml"));
        modelMap.put("templatesTypeMapping.xml", getClass().getResource("/cdaepsos/templatesTypeMapping.xml"));
        return modelMap;
    }

    @Override
    public Map<String, URL> retrieveModels() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("cda.uml", getClass().getResource("/modelad2/cda.uml"));
        modelMap.put("medication.uml", getClass().getResource("/modelad2/medication.uml"));
        modelMap.put("pharm.uml", getClass().getResource("/modelad2/pharm.uml"));

        VocModelRetriever vocModelRetriever = new VocModelRetriever();
        Map<String, URL> vocModelMap = vocModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : vocModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        NblockModelRetriever nblockModelRetriever = new NblockModelRetriever();
        Map<String, URL> nblockModelsMap = nblockModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : nblockModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        DatatypesModelRetriever datatypesModelRetriever = new DatatypesModelRetriever();
        Map<String, URL> datatypesModelMap = datatypesModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : datatypesModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        InfrModelRetriever infrModelRetriever = new InfrModelRetriever();
        Map<String, URL> infrModelMap = infrModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : infrModelMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }

        CommonModelRetriever commonModelRetriever = new CommonModelRetriever();
        Map<String, URL> commonModelsMap = commonModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : commonModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }
        return modelMap;
    }
}
