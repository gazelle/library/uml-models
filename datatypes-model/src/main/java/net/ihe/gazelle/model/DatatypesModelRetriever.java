package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class DatatypesModelRetriever extends ModelRetriever{



    protected String getDatatypeUmlName(){
        return "/models/datatypes.uml";
    }

    protected String getDatatypeUmlKey(){
        return "datatypes.uml";
    }

    @Override
    public Map<String, URL> retrieveResources() {
        return null;
    }



    @Override
    public Map<String, URL> retrieveModels(){
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put(getDatatypeUmlKey(), getClass().getResource(getDatatypeUmlName()));
        CommonModelRetriever commonModelRetriever = new CommonModelRetriever();
        Map<String, URL> commonModelsMap = commonModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : commonModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }
        return modelMap;
    }
}
