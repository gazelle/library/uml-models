package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class InfrModelRetriever extends ModelRetriever{

    protected String getInfrUmlName(){
        return "/models/infr.uml";
    }

    protected String getInfrUmlKey(){
        return "infr.uml";
    }

    @Override
    public Map<String, URL> retrieveResources() {
        return null;
    }

    @Override
    public Map<String, URL> retrieveModels(){
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put(getInfrUmlKey(), getClass().getResource(getInfrUmlName()));
        DatatypesModelRetriever commonModelRetriever = new DatatypesModelRetriever();
        Map<String, URL> commonModelsMap = commonModelRetriever.retrieveModels();
        for (Map.Entry<String, URL> entry : commonModelsMap.entrySet()){
            String key = entry.getKey();
            if (modelMap.get(key) == null){
                modelMap.put(key, entry.getValue());
            }
        }
        return modelMap;
    }
}
