package net.ihe.gazelle.model;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public abstract class ModelRetriever {

    public abstract Map<String, URL> retrieveResources();

    public abstract Map<String, URL> retrieveModels();

    public Map<String, URL> retrieveUMLProfiles() {
        Map<String, URL> modelMap = new HashMap<String, URL>();
        modelMap.put("Ecore.profile.properties", getClass().getResource("/uml/profiles/Ecore.profile.properties"));
        modelMap.put("Ecore.profile.uml", getClass().getResource("/uml/profiles/Ecore.profile.uml"));
        modelMap.put("Standard.profile.properties", getClass().getResource("/uml/profiles/Standard.profile.properties"));
        modelMap.put("Standard.profile.uml", getClass().getResource("/uml/profiles/Standard.profile.uml"));
        modelMap.put("UML2.profile.properties", getClass().getResource("/uml/profiles/UML2.profile.properties"));
        modelMap.put("UML2.profile.uml", getClass().getResource("/uml/profiles/UML2.profile.uml"));
        modelMap.put("common-profile.uml", getClass().getResource("/uml/profiles/common-profile.uml"));
        modelMap.put("cda-profiles.uml", getClass().getResource("/uml/profiles/cda-profiles.uml"));
        return modelMap;
    }
}
